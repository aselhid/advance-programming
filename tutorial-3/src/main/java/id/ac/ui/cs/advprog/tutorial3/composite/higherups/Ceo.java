package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public static final String ROLE = "CEO";
    public static final double MIN_SALARY = 200000.0;

    public Ceo(String name, double salary) {
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException();
        }

        this.name = name;
        this.salary = salary;
        this.role = ROLE;
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
