package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Cheese.
 */
public class Cheese extends Filling {
    public Cheese(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + ", adding cheese";
    }

    @Override
    public double cost() {
        return this.food.cost() + 2.0;
    }
}