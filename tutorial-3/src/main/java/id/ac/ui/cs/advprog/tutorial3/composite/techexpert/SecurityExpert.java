package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    public static final String ROLE = "Security Expert";
    public static final double MIN_SALARY = 70000.0;

    public SecurityExpert(String name, double salary) {
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException();
        }

        this.name = name;
        this.salary = salary;
        this.role = ROLE;
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
