package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Lettuce.
 */
public class Lettuce extends Filling {
    public Lettuce(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + ", adding lettuce";
    }

    @Override
    public double cost() {
        return this.food.cost() + 0.75;
    }
}