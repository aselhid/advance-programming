package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Cucumber.
 */
public class Cucumber extends Filling {
    public Cucumber(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + ", adding cucumber";
    }

    @Override
    public double cost() {
        return this.food.cost() + 0.4;
    }
}