package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * ChiliSauce.
 */
public class ChiliSauce extends Filling {
    public ChiliSauce(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + ", adding chili sauce";
    }

    @Override
    public double cost() {
        return this.food.cost() + 0.3;
    }

}