package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public static final String ROLE = "CTO";
    public static final double MIN_SALARY = 100000.0;

    public Cto(String name, double salary) {
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException();
        }

        this.name = name;
        this.salary = salary;
        this.role = ROLE;
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
