package tutorial.javari.animal;

public class AnimalRequest {
    private String type;
    private String name;
    private String gender;
    private double length;
    private double weight;
    private String condition;

    public AnimalRequest() {
        this.type = "";
        this.name = "";
        this.gender = "male";
        this.length = 0;
        this.weight = 0;
        this.condition = "healthy";
    }

    public AnimalRequest(String type, String name, String gender, double length,
                  double weight, String condition) {
        this.type = type;
        this.name = name;
        this.gender = gender;
        this.length = length;
        this.weight = weight;
        this.condition = condition;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
