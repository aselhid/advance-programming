package tutorial.javari;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.AnimalRequest;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

public class AnimalModel {
    private static final String path = "tutorial-9/src/main/java/tutorial/javari/animal_model.csv";
    private ArrayList<Animal> animals;

    public AnimalModel() {
        try {
            BufferedReader fileReader = new BufferedReader(new FileReader(path));
            CSVParser csvParser = new CSVParser(
                    fileReader,
                    CSVFormat.DEFAULT
                            .withFirstRecordAsHeader()
                            .withIgnoreHeaderCase()
                            .withTrim()
            );

            animals = new ArrayList<>();
            Iterator<CSVRecord> csvRecords = csvParser.getRecords().iterator();

            while (csvRecords.hasNext()) {
                CSVRecord record = csvRecords.next();
                int id = Integer.parseInt(record.get("id"));
                String type = record.get("type");
                String name = record.get("name");
                Gender gender = Gender.parseGender(record.get("gender"));
                double length = Double.parseDouble(record.get("length"));
                double weight = Double.parseDouble(record.get("weight"));
                Condition condition = Condition.parseCondition(record.get("condition"));

                Animal animal = new Animal(id, type, name, gender, length, weight, condition);
                animals.add(animal);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Animal> getAnimals() {
        return animals;
    }

    public Animal getAnimalById(int id) {
        Animal targetAnimal = null;

        for (Animal animal: this.animals) {
            if (animal.getId() == id) {
                targetAnimal = animal;
                break;
            }
        }

        return targetAnimal;
    }

    public Animal addAnimal(AnimalRequest animalRequest) {
        int id = this.animals.size() == 0
                ? 0 : this.animals.get(this.animals.size() - 1).getId() + 1;
        Animal animal = new Animal(
                id,
                animalRequest.getType(),
                animalRequest.getName(),
                Gender.parseGender(animalRequest.getGender()),
                animalRequest.getLength(),
                animalRequest.getWeight(),
                Condition.parseCondition(animalRequest.getCondition())
        );

        try {
            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(path, true));
            CSVPrinter csvPrinter = new CSVPrinter(
                    fileWriter,
                    CSVFormat.DEFAULT
                            .withFirstRecordAsHeader()
                            .withTrim()
                            .withIgnoreHeaderCase()
            );
            csvPrinter.printRecord(
                    animal.getId(),
                    animal.getType(),
                    animal.getName(),
                    animal.getGender(),
                    animal.getLength(),
                    animal.getWeight(),
                    animal.getCondition()
            );

            csvPrinter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.animals.add(animal);
        return animal;
    }

    public boolean deleteById(int id) {
        int count = this.animals.size();

        this.animals = this.animals
                .stream()
                .filter(animal -> animal.getId() != id)
                .collect(Collectors.toCollection(ArrayList::new));

        if (count == this.animals.size()) {
            return false;
        }

        try {
            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(path));
            CSVPrinter csvPrinter = new CSVPrinter(
                    fileWriter,
                    CSVFormat.DEFAULT
                            .withFirstRecordAsHeader()
                            .withTrim()
                            .withIgnoreHeaderCase()
            );

            csvPrinter.printRecord("id","type","name","gender","length","weight","condition");
            for (Animal animal : this.animals) {
                csvPrinter.printRecord(
                        animal.getId(),
                        animal.getType(),
                        animal.getName(),
                        animal.getGender(),
                        animal.getLength(),
                        animal.getWeight(),
                        animal.getCondition()
                );
            }
            csvPrinter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }
}
