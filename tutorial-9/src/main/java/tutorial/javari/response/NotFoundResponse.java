package tutorial.javari.response;

public class NotFoundResponse {
    private String message;

    public NotFoundResponse() {
        this.message = "NOT FOUND";
    }

    public NotFoundResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
