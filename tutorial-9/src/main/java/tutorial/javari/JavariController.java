package tutorial.javari;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.AnimalRequest;
import tutorial.javari.exception.NotFoundException;
import tutorial.javari.response.NotFoundResponse;

@ControllerAdvice
@RestController
@RequestMapping("/javari")
public class JavariController {
    private AnimalModel animalModel = new AnimalModel();

    @RequestMapping(method = RequestMethod.GET)
    public ArrayList<Animal> getAll() throws NotFoundException {
        ArrayList<Animal> animals = animalModel.getAnimals();

        if (animals.size() > 0) {
            return animals;
        }

        throw new NotFoundException("No animals");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Animal getById(@PathVariable("id") int id) throws NotFoundException {
        Animal animal = animalModel.getAnimalById(id);

        if (animal != null) {
            return animal;
        }

        throw new NotFoundException(id + " not found");
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Animal> create(@RequestBody AnimalRequest animalRequest) {
        Animal animal = this.animalModel.addAnimal(animalRequest);

        return new ResponseEntity<>(animal, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable int id) throws NotFoundException {
        boolean flag = this.animalModel.deleteById(id);

        if (flag) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        throw new NotFoundException(id + " not found");
    }

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<NotFoundResponse> notFoundHandler(NotFoundException e) {
        return new ResponseEntity<>(new NotFoundResponse(e.getMessage()), HttpStatus.NOT_FOUND);
    }
}
