package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

/**
 * WhiteSauce.
 */
public class WhiteSauce implements Sauce {
    @Override
    public String toString() {
        return "Sauce with cream";
    }

}