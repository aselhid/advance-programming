package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

/**
 * SaucedClams.
 */
public class SaucedClams implements Clams {
    @Override
    public String toString() {
        return "Clams but with sauce";
    }

}