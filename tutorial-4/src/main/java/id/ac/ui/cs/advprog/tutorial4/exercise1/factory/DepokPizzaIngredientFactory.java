package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.GoudaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.SaucedClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.GlutenFreeDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.WhiteSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Corn;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

/**
 * DepokPizzaIngredientFactory.
 */
public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new GlutenFreeDough();
    }

    @Override
    public Cheese createCheese() {
        return new GoudaCheese();
    }

    @Override
    public Clams createClam() {
        return new SaucedClams();
    }

    @Override
    public Sauce createSauce() {
        return new WhiteSauce();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies[] veggies = { new Eggplant(), new Onion(), new Spinach(), new Corn() };
        return veggies;
    }

}