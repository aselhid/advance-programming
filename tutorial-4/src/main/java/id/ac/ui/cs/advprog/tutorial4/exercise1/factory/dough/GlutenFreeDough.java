package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

/**
 * GlutenFreeDough.
 */
public class GlutenFreeDough implements Dough {
    @Override
    public String toString() {
        return "Gluten free dough";
    }

}