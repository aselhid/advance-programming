package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {

    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum = 0.0f;
    private int numReadings;

    public StatisticsDisplay(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void display() {
        System.out.print("Avg/Max/Min temperature = ");
        System.out.println((tempSum / numReadings) + "/" + maxTemp + "/" + minTemp);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData weatherData = (WeatherData) o;
            float currentTemp = weatherData.getTemperature();
            this.numReadings++;

            this.tempSum += currentTemp;
            this.maxTemp = currentTemp > this.maxTemp ? currentTemp : maxTemp;
            this.minTemp = currentTemp < this.minTemp ? currentTemp : minTemp;
            display();
        }
    }
}
