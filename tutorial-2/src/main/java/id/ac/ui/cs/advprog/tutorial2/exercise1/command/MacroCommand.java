package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        this.commands.stream().forEach((command) -> command.execute());
    }

    @Override
    public void undo() {
        int commandsLength = this.commands.size();
        ListIterator<Command> iterator = this.commands.listIterator(commandsLength);

        while (iterator.hasPrevious()) {
            Command currentCmd = iterator.previous();
            currentCmd.undo();
        }
    }
}
