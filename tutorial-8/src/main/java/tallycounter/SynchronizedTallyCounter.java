package tallycounter;

public class SynchronizedTallyCounter extends TallyCounter {
    private int counter = 0;

    @Override
    public synchronized void increment() {
        counter++;
    }

    @Override
    public synchronized void decrement() {
        counter--;
    }

    @Override
    public synchronized int value() {
        return counter;
    }
}
