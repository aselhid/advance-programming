package game;

import java.util.TimerTask;

public class DecrementScoreTask extends TimerTask {
    private Score score;

    DecrementScoreTask(Score score) {
        this.score = score;
    }

    @Override
    public void run() {
        score.substract(1);
    }
}
