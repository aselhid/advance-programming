package game;

public class Score {
    private int score;

    Score(int initialValue) {
        score = initialValue;
    }

    public synchronized void add(int value) {
        score += value;
    }

    public synchronized void substract(int value) {
        score -= value;
    }

    public synchronized void addByPercentage(int percentage) {
        score += score * percentage / 100;
    }

    public synchronized void substractByPercentage(int percentage) {
        score -= score * percentage / 100;
    }

    public synchronized int get() {
        return score;
    }
}
